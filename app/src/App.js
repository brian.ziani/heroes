
import React, { useEffect, useState } from 'react';
import { Grommet, Box, Table, TableHeader, TableRow, TableCell, TableBody, Avatar, Accordion, AccordionPanel, Text } from 'grommet';
const theme = {
  global: {
    font: {
      family: 'Roboto',
      size: '18px',
      height: '20px',
    },
  },
};
const  App = () => {
    // eslint-disable-next-line no-unused-vars
    const url = 'https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/all.json' // to replace by env var on production
    const [data, setData] = useState([]);

    useEffect(() => {
      getHeroesData()
    }, []);
    

    const getHeroesData = async () =>{
      await fetch(url)
            .then((res) => res.json())
            .then((heroes) => setData(heroes));
    }

    data.map((data) => console.log(data));
  return (
    <Grommet theme={theme}>
      <Box   direction="row" pad="large">
        <Table>
          <TableHeader>
            <TableRow>
              <TableCell scope="col" border="bottom">
                Name
              </TableCell>
              <TableCell scope="col" border="bottom">
                FullName
              </TableCell>
              <TableCell scope="col" border="bottom">
                Image
              </TableCell>
              <TableCell scope="col" border="bottom">
                Stats
              </TableCell>
              <TableCell scope="col" border="bottom">
                Alignement
              </TableCell>
              <TableCell scope="col" border="bottom">
                Appearance
              </TableCell>
            </TableRow>
          </TableHeader>
          <TableBody>
          {
            data.map(
              (data) =>  <TableRow>
                                          
                <TableCell>
                  {data.name}
                </TableCell>
                <TableCell>
                  {data.biography.fullName}
                </TableCell>
                <TableCell>
                  <Box size="medium" align="center" justify="center" overflow="hidden" round="full">
                    <Avatar src={data.images.sm}/>
                  </Box>
                </TableCell>
                <TableCell>
                  <Accordion>
                    <AccordionPanel label="consult stats">
                      <Box background='light-2'>
                        <Text>Combat : {data.powerstats.combat}</Text>
                      </Box>
                      <Box background='light-2'>
                        <Text>Durability : {data.powerstats.durability}</Text>
                      </Box>
                      <Box background='light-2'>
                        <Text>Intelligence : {data.powerstats.intelligence}</Text>
                      </Box>
                      <Box background='light-2'>
                        <Text>Power : {data.powerstats.power}</Text>
                      </Box>
                      <Box background='light-2'>
                        <Text>Speed : {data.powerstats.speed}</Text>
                      </Box>
                      <Box background='light-2'>
                        <Text>Strength : {data.powerstats.strength}</Text>
                      </Box>
                    </AccordionPanel>
                  </Accordion>
                </TableCell>
                <TableCell>
                  {data.biography.alignment}
                </TableCell>
              </TableRow>
              
            )
          }
          </TableBody>
        </Table>
      </Box>
    </Grommet>
  );
}

export default App;
